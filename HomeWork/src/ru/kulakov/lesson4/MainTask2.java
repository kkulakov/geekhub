package ru.kulakov.lesson4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

public class MainTask2 {
    public static void main(String[] args) {
        TaskManager taskManager = new TaskManagerImpl();
        Scanner scan = new Scanner(System.in);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");

        System.out.println("Welcome to the task manager!");
        while (true) {
            System.out.println("\n Enter a number: \n1 to add task;\n2 to remove;" +
                    "\n3 to view categories; \n4 to view all tasks, sorted by categories;" +
                    "\n5 to view tasks in some category, \n6 to view tasks for today; \n7 to exit.");
            try {
                switch (Integer.parseInt(scan.nextLine())) {
                    case 1:
                        System.out.println("\nPlease, enter a date as dd.mm.yyyy hh:mm:");
                        Date date = dateFormat.parse(scan.nextLine());
                        System.out.println("Please, enter a category of the task:");
                        String category = scan.nextLine();
                        System.out.println("Please, enter a description of the task:");
                        String description = scan.nextLine();
                        taskManager.addTask(date, new Task(category, description));
                        System.out.println("Task was added successfully!");
                        break;
                    case 2:
                        System.out.println("\nPlease, enter a date for a task, you want to remove as dd.mm.yyyy hh:mm:");
                        taskManager.removeTask(dateFormat.parse(scan.nextLine()));
                        break;
                    case 3:
                        Collection<String> categories = taskManager.getCategories();
                        System.out.println("\n\nCategories:");
                        for (String c : categories) {
                            System.out.println(c);
                        }
                        break;
                    case 4:
                        Map<String, Map<Date, Task>> tasksByCategories = taskManager.getTasksByCategories();
                        for (Map.Entry<String, Map<Date, Task>> mapEntry : tasksByCategories.entrySet()) {
                            System.out.println("Category:" + mapEntry.getKey());
                            for (Map.Entry<Date, Task> taskEntry : mapEntry.getValue().entrySet()) {
                                System.out.println("\t" + dateFormat.format(taskEntry.getKey()) + "\t" + taskEntry.getValue().getDescription());
                            }
                        }
                        break;
                    case 5:
                        System.out.println("\nPlease, enter a category:");
                        String c = scan.nextLine();
                        Map<Date, Task> tasksByCategory = taskManager.getTasksByCategory(c);
                        System.out.println("Tasks for this category:");
                        for (Map.Entry<Date, Task> taskEntry : tasksByCategory.entrySet()) {
                            System.out.println("\t" + dateFormat.format(taskEntry.getKey()) + "\t"
                                    + taskEntry.getValue().getDescription());
                        }
                        break;
                    case 6:
                        System.out.println("\nTasks for today:");
                        Map<Date, Task> tasksForToday = taskManager.getTasksForDay();
                        for (Map.Entry<Date, Task> taskEntry : tasksForToday.entrySet()) {
                            System.out.println("\t" + dateFormat.format(taskEntry.getKey()) + "\t"
                                    + taskEntry.getValue().getDescription());
                        }
                        break;
                    case 7:
                        System.out.println("\nGoodbye! Hope to see you again!");
                        return;
                    default:
                        System.out.println("There aren't any variant with such number. Try again:");
                }
            } catch (NumberFormatException exception) {
                System.out.println("Only numbers is allowed! Try again:");
            } catch (ParseException exception) {
                System.out.println("Date is wrong! Please enter a date as dd.mm.yyyy hh:mm.");
            }
        }
    }
}
