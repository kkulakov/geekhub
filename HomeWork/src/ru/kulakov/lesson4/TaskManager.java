package ru.kulakov.lesson4;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskManager {
    public void addTask(Date date, Task task);

    public void removeTask(Date date);

    public Collection<String> getCategories();

    //for nex 3 methods tasks should be sorted by scheduled date
    public Map<String, Map<Date, Task>> getTasksByCategories();

    public Map<Date, Task> getTasksByCategory(String category);

    public Map<Date, Task> getTasksForDay();
}
