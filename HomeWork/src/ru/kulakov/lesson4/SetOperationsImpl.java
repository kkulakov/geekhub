package ru.kulakov.lesson4;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

public class SetOperationsImpl implements SetOperations {

    public boolean equals(Set a, Set b) {
        if (a.size() != b.size()) {
            return false;
        }

        return a.containsAll(b);
    }

    public Set union(Set a, Set b) {
        Set result = getSetObject(a);
        result.addAll(a);

        if (equals(a, b)) {
            return result;
        } else {
            result.addAll(b);
            return result;
        }
    }

    public Set subtract(Set a, Set b) {
        Set result = getSetObject(a);
        if (a.containsAll(b)) {
            return result;
        }

        result.addAll(a);
        result.removeAll(b);

        return result;
    }

    public Set intersect(Set a, Set b) {
        Set result = getSetObject(a);
        result.addAll(a);

        if (equals(a, b)) {
            return result;
        }

        result.removeAll(subtract(a, b));
        return result;
    }

    public Set symmetricSubtract(Set a, Set b) {
        if (equals(a, b)) {
            return getSetObject(a);
        }
        return union(subtract(a, b), subtract(b, a));
    }

    private Set getSetObject(Set set) {
        Set result = null;
        try {
            result = set.getClass().getConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InvocationTargetException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (NoSuchMethodException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        assert (result != null);
        return result;
    }
}
