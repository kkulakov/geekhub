package ru.kulakov.lesson4;

import java.text.SimpleDateFormat;
import java.util.*;

public class TaskManagerImpl implements TaskManager {
    Map<Date, Task> tasks = new TreeMap<Date, Task>();

    public void addTask(Date date, Task task) {
        tasks.put(date, task);
    }

    public void removeTask(Date date) {
        tasks.remove(date);
    }

    public Collection<String> getCategories() {
        Set<String> categories = new HashSet<String>();
        for (Task t : tasks.values()) {
            categories.add(t.getCategory());
        }
        return categories;
    }

    public Map<String, Map<Date, Task>> getTasksByCategories() {
        Map<String, Map<Date, Task>> result = new HashMap<String, Map<Date, Task>>();
        Collection<String> categories = getCategories();
        for (String category : categories) {
            result.put(category, getTasksByCategory(category));
        }
        return result;
    }

    public Map<Date, Task> getTasksByCategory(String category) {
        Map<Date, Task> result = new TreeMap<Date, Task>();
        for (Map.Entry<Date, Task> taskEntry : tasks.entrySet()) {
            if (taskEntry.getValue().getCategory().equals(category)) {
                result.put(taskEntry.getKey(), taskEntry.getValue());
            }
        }
        return result;
    }

    public Map<Date, Task> getTasksForDay() {
        Map<Date, Task> result = new TreeMap<Date, Task>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String today = dateFormat.format(new Date());

        for (Map.Entry<Date, Task> taskEntry : tasks.entrySet()) {
            if (dateFormat.format(taskEntry.getKey()).equals(today)) {
                result.put(taskEntry.getKey(), taskEntry.getValue());
            }
        }
        return result;
    }
}
