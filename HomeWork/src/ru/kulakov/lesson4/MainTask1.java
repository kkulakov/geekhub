package ru.kulakov.lesson4;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class MainTask1 {
    private static final int SET_SIZE = 3;
    private static final int VALUES_BOUNDS = 10;

    public static void main(String[] args) {
        SetOperations setOperations = new SetOperationsImpl();
        Set<Integer> firstSet = new HashSet<Integer>();
        Set<Integer> secondSet = new HashSet<Integer>();

        fillSet(firstSet);
        fillSet(secondSet);

        showSet("First set:", firstSet);
        showSet("Second set:", secondSet);

        System.out.println("Are sets equal: " + setOperations.equals(firstSet, secondSet) + "\n");
        showSet("Union:", setOperations.union(firstSet, secondSet));
        showSet("Subtract:", setOperations.subtract(firstSet, secondSet));
        showSet("Intersect:", setOperations.intersect(firstSet, secondSet));
        showSet("Symmetric subtract:", setOperations.symmetricSubtract(firstSet, secondSet));
    }


    private static void fillSet(Set set) {
        for (int i = 0; i < SET_SIZE; i++) {
            set.add(new Integer((int) (Math.random() * VALUES_BOUNDS)));
        }
    }

    private static <T> void showSet(String s, Set<T> set) {
        System.out.println(s);
        Iterator iterator = set.iterator();
        for (int i = 1; iterator.hasNext(); i++) {
            System.out.println("Element №" + i + ":" + (T) iterator.next());
        }
        System.out.println();
    }
}
