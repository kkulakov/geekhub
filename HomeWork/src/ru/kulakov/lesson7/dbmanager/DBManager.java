package ru.kulakov.lesson7.dbmanager;

import java.sql.*;


public class DBManager implements AutoCloseable {

    final private int TEXTFIELD_SIZE = 13;
    private Connection connection;

    public DBManager(String hostName, String dbName, String login, String password) throws SQLException {
        connection = DriverManager.getConnection(hostName + dbName, login, password);
    }

    @Override
    public void close() throws SQLException {
        connection.close();
    }

    public void executeQuery(String sql) throws SQLException {
        if (sql == null || sql.isEmpty() || sql.length() < 6)
            throw new SQLException("SQL query is empty or wrong!");

        if (!"select".equalsIgnoreCase(sql.substring(0, 6))) {
            updateExecuteUpdate(sql);
            return;
        }
        try (Statement stmt = connection.createStatement()) {
            ResultSet result = stmt.executeQuery(sql);
            printTable(result);
        }
    }

    private void updateExecuteUpdate(String command) throws SQLException {
        try (Statement stmt = connection.createStatement()) {
            System.out.printf("The number of modified records is %d%n", stmt.executeUpdate(command));
        }
    }

    private void printTable(ResultSet result) throws SQLException {

        ResultSetMetaData resultMetaData = result.getMetaData();
        System.out.print("|");

        for (int i = 0; i < resultMetaData.getColumnCount(); i++) {
            System.out.printf("%" + TEXTFIELD_SIZE + "s|", resultMetaData.getColumnName(i + 1));
        }
        System.out.print("\n");

        for (int i = 0; i < resultMetaData.getColumnCount() * (TEXTFIELD_SIZE + 1); i++) {
            System.out.print("=");
        }
        System.out.print("\n");

        while (result.next()) {
            System.out.print("|");
            for (int i = 0; i < resultMetaData.getColumnCount(); i++) {
                Object record = result.getObject(i + 1);
                if (!result.wasNull()) {
                    if (record.toString().length() > 10)
                        System.out.printf("%" + (TEXTFIELD_SIZE - 3) + "s...|", record.toString().substring(0, 9));
                    else
                        System.out.printf("%" + TEXTFIELD_SIZE + "s|", record.toString());
                } else {
                    System.out.printf("%" + TEXTFIELD_SIZE);
                }
            }
            System.out.print("\n");
        }

        for (int i = 0; i < resultMetaData.getColumnCount() * (TEXTFIELD_SIZE + 1); i++) {
            System.out.print("=");
        }
        System.out.print("\n");
    }

}
