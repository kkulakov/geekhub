package ru.kulakov.lesson7.dbstorage.storage;

import ru.kulakov.lesson7.dbstorage.objects.Entity;
import ru.kulakov.lesson7.dbstorage.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * Implementation of {@link ru.kulakov.lesson7.dbstorage.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link ru.kulakov.lesson7.dbstorage.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            return (statement.executeUpdate(sql) > 0);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        String sql = null;
        if (entity.isNew()) {
            String values = " VALUES (";
            sql = "INSERT INTO " + entity.getClass().getSimpleName() + " (";

            for (Map.Entry<String, Object> entry : data.entrySet()) {
                sql += entry.getKey() + ", ";
                values += convertToString(entry.getValue()) + ", ";
            }
            sql = sql.substring(0, (sql.length() - 2)) + ") " + values.substring(0, (values.length() - 2)) + ")";
        } else {
            sql = "UPDATE " + entity.getClass().getSimpleName() + " SET ";
            for (Map.Entry<String, Object> entry : data.entrySet()) {
                sql += entry.getKey() + "=" + convertToString(entry.getValue()) + ", ";
            }
            sql = sql.substring(0, (sql.length() - 2)) + " WHERE id = " + entity.getId();
        }

        //save/update object and update it with new id if it's a creation
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

            //setting id for the added entity
            if (entity.isNew()) {
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next())
                    entity.setId(generatedKeys.getInt(1));
                else
                    throw new SQLException("Can't get object's id.");
            }
        }
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Field[] declaredFields = entity.getClass().getDeclaredFields();
        Map<String, Object> resultMap = new HashMap<>();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(Ignore.class))
                continue;
            field.setAccessible(true);
            resultMap.put(field.getName(), field.get(entity));
            field.setAccessible(false);
        }
        return resultMap;
    }

    //creates list of new instances of clazz by using data from resultSet
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> resultList = new ArrayList<>();
        Field[] declaredFields = clazz.getDeclaredFields();
        while (resultSet.next()) {
            T entity = clazz.newInstance();

            entity.setId(resultSet.getInt("id"));
            for (Field field : declaredFields) {
                if (field.isAnnotationPresent(Ignore.class))
                    continue;
                field.setAccessible(true);
                field.set(entity, resultSet.getObject(field.getName(), field.getType()));
                field.setAccessible(false);
            }

            resultList.add(entity);
        }
        return resultList;
    }

    private String convertToString(Object entry) {
        Set<Class> numericTypes = new HashSet<Class>(Arrays.asList(
                Integer.class,
                Short.class,
                Long.class,
                Byte.class,
                Double.class,
                Float.class,
                Boolean.class,
                int.class,
                short.class,
                long.class,
                byte.class,
                double.class,
                float.class,
                boolean.class
        ));
        return numericTypes.contains(entry.getClass()) ? entry.toString() : "'" + entry.toString() + "'";
    }
}
