package ru.kulakov.lesson2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Drivable vehicle;

        System.out.println("Which type of vehicle would you choose today?");
        System.out.println("Enter 1 for car, 2 for solar powered car and 3 for boat.");
        Scanner scan = new Scanner(System.in);
        while (true) {
            try {
                int choice = Integer.parseInt(scan.nextLine());
                if (choice == 1) {
                    vehicle = new Car();
                    break;
                } else if (choice == 2) {
                    vehicle = new SolarPoweredCar();
                    break;
                } else if (choice == 3) {
                    vehicle = new Boat();
                    break;
                }
                System.out.println("There aren't any variant with such number. Try again:");
            } catch (NumberFormatException exception) {
                System.out.println("Only numbers is allowed! Try again:");
            }
        }
        System.out.println("Okay, today we will travel with a " + vehicle.getClass().getSimpleName() + ".");
        while (true) {
            System.out.println("What will we do next?");
            System.out.println("Enter 1 to accelerate vehicle, 2 to push brake, 3 to turn vehicle and 4 to exit.");
            try {
                switch (Integer.parseInt(scan.nextLine())) {
                    case 1:
                        System.out.println("Enter the power of acceleration (power is equal speed):");
                        vehicle.accelerate(Integer.parseInt(scan.nextLine()));
                        break;
                    case 2:
                        System.out.println("Enter the power of pushing brake pedal (power is equal to speed loss):");
                        vehicle.brake(Integer.parseInt(scan.nextLine()));
                        break;
                    case 3:
                        System.out.println("Enter the angle of the wheels' rotation (from -90 to 90 degrees):");
                        vehicle.turn(Integer.parseInt(scan.nextLine()));
                        break;
                    case 4:
                        System.out.println("Goodbye! Hope to see you again!");
                        return;
                    default:
                        System.out.println("There aren't any variant with such number. Try again:");
                }
            } catch (NumberFormatException exception) {
                System.out.println("Only numbers is allowed! Try again:");
            }
        }
    }
}
