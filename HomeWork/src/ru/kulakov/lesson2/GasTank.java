package ru.kulakov.lesson2;

public class GasTank implements EnergyProvider {
    private int energyAmount = 50;
    private final int MAX_AMOUNT = 60;

    public boolean getEnergy(int amount, ControlPanel controlPanel) {
        if (energyAmount - amount >= 0) {
            energyAmount -= amount;
            return true;
        }
        energyAmount = 0;
        controlPanel.showMessage("The gas tank is empty!");
        return false;
    }

    public boolean addEnergy(int amount, ControlPanel controlPanel) {
        if (energyAmount + amount <= MAX_AMOUNT) {
            energyAmount += amount;
        } else {
            energyAmount = MAX_AMOUNT;
            controlPanel.showMessage("The gas tank is full.");
        }
        return false;
    }
}
