package ru.kulakov.lesson2;

public class ControlPanel {
    public void showSpeed(int speed) {
        if (speed != 0){
            System.out.println("Current speed is " + speed + " km/h.");
        }
        else{
            System.out.println("You aren't moving now.");
        }
    }

    public void showDirection(int angle) {
        System.out.println("Current direction is " + angle + " angle(s).");
    }

    public void showMessage(String message) {
        System.out.println(message);
    }
}
