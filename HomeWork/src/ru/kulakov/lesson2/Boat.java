package ru.kulakov.lesson2;

public class Boat extends Vehicle {
    Boat() {
        super(new GasTank(), new Propeller());
    }
}
