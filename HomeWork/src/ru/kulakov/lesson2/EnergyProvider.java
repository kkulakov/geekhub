package ru.kulakov.lesson2;

public interface EnergyProvider {
    public boolean getEnergy(int amount, ControlPanel controlPanel);

    public boolean addEnergy(int amount, ControlPanel controlPanel);
}

