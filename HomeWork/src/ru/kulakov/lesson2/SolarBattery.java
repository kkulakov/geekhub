package ru.kulakov.lesson2;

public class SolarBattery implements EnergyProvider {
    public boolean getEnergy(int amount, ControlPanel controlPanel) {
        return true; //The energy is infinite! Take as much as you want!
    }

    public boolean addEnergy(int amount, ControlPanel controlPanel) {
        controlPanel.showMessage("The fuel is infinite and you don't need to add it.");
        return true;
    }
}
