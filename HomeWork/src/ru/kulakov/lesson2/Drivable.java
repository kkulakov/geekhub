package ru.kulakov.lesson2;

public interface Drivable {
    public void accelerate(int power);

    public void brake(int power);

    public void turn(int angle);
}
