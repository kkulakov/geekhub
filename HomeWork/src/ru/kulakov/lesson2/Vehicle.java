package ru.kulakov.lesson2;

public abstract class Vehicle implements Drivable {
    protected int currentSpeed;
    protected int currentDirection;
    protected Engine engine = new Engine();
    protected ControlPanel controlPanel = new ControlPanel();
    protected EnergyProvider energyProvider;
    protected ForceProvider forceProvider;

    Vehicle(EnergyProvider energyProvider, ForceProvider forceProvider) {
        this.energyProvider = energyProvider;
        this.forceProvider = forceProvider;
    }

    @Override
    public void accelerate(int power) {
        currentSpeed = engine.accelerate(power, currentSpeed, energyProvider, controlPanel);
        controlPanel.showSpeed(currentSpeed);
    }

    @Override
    public void brake(int power) {
        currentSpeed = forceProvider.brake(power, currentSpeed, controlPanel);
        controlPanel.showSpeed(currentSpeed);
    }

    @Override
    public void turn(int angle) {
        currentDirection = forceProvider.turn(angle, currentDirection, controlPanel);
        controlPanel.showDirection(currentDirection);
    }
}
