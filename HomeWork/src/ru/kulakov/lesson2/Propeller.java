package ru.kulakov.lesson2;

public class Propeller implements ForceProvider {
    private final int CIRCLE = 360;

    public int turn(int angle, int currentAngle, ControlPanel controlPanel) {
        if (angle > 90 || angle < -90) {
            controlPanel.showMessage("The turning angle is incorrect! Wheels can't turn more then for 90 degrees in each side.");
        }

        if ((currentAngle + angle >= 0) && (currentAngle + angle <= CIRCLE)) {
            return currentAngle + angle;
        } else if (currentAngle + angle < 0) {
            return CIRCLE + (currentAngle + angle);
        } else if (currentAngle + angle > CIRCLE) {
            return (currentAngle + angle) - CIRCLE;
        }

        return currentAngle;
    }

    public int brake(int power, int currentSpeed, ControlPanel controlPanel) {
        controlPanel.showMessage("Sorry, but boats hasn't any brake. You can wait until boat will stop itself.");
        return currentSpeed - 1;
    }
}
