package ru.kulakov.lesson2;

public class SolarPoweredCar extends Vehicle {
    SolarPoweredCar() {
        super(new SolarBattery(), new Wheel());
    }

}
