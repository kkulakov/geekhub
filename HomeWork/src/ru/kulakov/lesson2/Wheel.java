package ru.kulakov.lesson2;

public class Wheel implements ForceProvider {
    private final int CIRCLE = 360;

    public int turn(int angle, int currentAngle, ControlPanel controlPanel) {
        if (angle > 90 || angle < -90) {
            controlPanel.showMessage("The turning angle is incorrect! Wheels can't turn more then for 90 degrees in each side.");
        }

        if ((currentAngle + angle >= 0) && (currentAngle + angle <= CIRCLE)) {
            return currentAngle + angle;
        } else if (currentAngle + angle < 0) {
            return CIRCLE + (currentAngle + angle);
        } else if (currentAngle + angle > CIRCLE) {
            return (currentAngle + angle) - CIRCLE;
        }

        return currentAngle;
    }

    public int brake(int power, int currentSpeed, ControlPanel controlPanel) {
        if (power > 0 && currentSpeed - power > 0) {
            return currentSpeed - power;
        } else if (currentSpeed - power < 0) {
            return 0;
        }
        controlPanel.showMessage("Can't slow down vehicle. Was entered power correct?");
        return currentSpeed;
    }
}
