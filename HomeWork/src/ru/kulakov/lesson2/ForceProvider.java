package ru.kulakov.lesson2;

public interface ForceProvider {
    public int turn(int angle, int currentAngle, ControlPanel controlPanel);

    public int brake(int power, int currentSpeed, ControlPanel controlPanel);
}
