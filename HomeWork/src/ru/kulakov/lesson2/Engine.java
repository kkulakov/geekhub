package ru.kulakov.lesson2;

public class Engine {
    private final int MAX_SPEED = 100;

    public int accelerate(int power, int currentSpeed, EnergyProvider provider, ControlPanel controlPanel) {
        if (provider.getEnergy(1, controlPanel) && power > 0 && currentSpeed + power < MAX_SPEED) {
            return currentSpeed + power;
        } else if (currentSpeed + power > MAX_SPEED) {
            return MAX_SPEED;
        }
        controlPanel.showMessage("Can't accelerate vehicle. Was entered power correct?");
        return currentSpeed;
    }

}
