package ru.kulakov.lesson3;

public class SpeedExcessException extends Exception {
    private int maxSpeed;

    public SpeedExcessException(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String getMessage() {
        return "This car can't go faster then " + maxSpeed + "km/h. You've reached maximum speed.";
    }
}
