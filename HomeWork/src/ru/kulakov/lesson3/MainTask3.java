package ru.kulakov.lesson3;

import java.util.Scanner;

public class MainTask3 {
    public static void main(String[] args) {
        CarTask3 car = new CarTask3();
        Scanner scan = new Scanner(System.in);

        while (true) {
            System.out.println("What would you do?");
            System.out.println("Press 1 to accelerate car, 2 to push brake, 3 to drive, 4 to tank up and 5 to exit:");
            try {
                switch (Integer.parseInt(scan.nextLine())) {
                    case 1:
                        System.out.println("Enter the power of acceleration (power is equal speed):");
                        System.out.println("Current speed is " +
                                car.accelerate(Integer.parseInt(scan.nextLine())) + " km/h");
                        break;
                    case 2:
                        System.out.println("Enter the power of pushing brake pedal (power is equal to speed loss):");
                        System.out.println("Current speed is " +
                                car.brake(Integer.parseInt(scan.nextLine())) + " km/h");
                        break;
                    case 3:
                        System.out.println("Enter distance, you want to drive:");
                        car.drive(Integer.parseInt(scan.nextLine()));
                        System.out.println("The distance was driven successfully. There are " +
                                car.getGasAmount() + " l of gas left.");
                        break;
                    case 4:
                        System.out.println("Enter the amount of fuel, you want to tank up:");
                        car.tankUp(Integer.parseInt(scan.nextLine()));
                        System.out.println("The action was performed successfully.");
                        break;
                    case 5:
                        System.out.println("Goodbye! Hope to see you again!");
                        return;
                    default:
                        System.out.println("There aren't any variant with such number. Try again:");
                }
            } catch (NumberFormatException exception) {
                System.out.println("Only numbers is allowed! Try again:");
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }
    }
}
