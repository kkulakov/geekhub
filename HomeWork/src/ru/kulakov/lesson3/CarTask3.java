package ru.kulakov.lesson3;

public class CarTask3 {
    private final double TANK_CAPACITY = 60;
    private final double FUEL_CONSUMPTION_PER_100_KM = 11;
    private final int MAX_SPEED = 200;
    private int currentSpeed = 0;
    private double gasAmount = 50;

    public int accelerate(int power) throws SpeedExcessException, WrongDataException {
        if (currentSpeed + power <= MAX_SPEED && power > 0) {
            currentSpeed += power;
        } else if (power < 0) {
            throw new WrongDataException();
        } else {
            currentSpeed = MAX_SPEED;
            throw new SpeedExcessException(MAX_SPEED);
        }
        return currentSpeed;
    }

    public int brake(int power) throws BrakeException, WrongDataException {
        if (currentSpeed - power >= 0 && power > 0) {
            currentSpeed -= power;
        } else if (power < 0) {
            throw new WrongDataException();
        } else {
            currentSpeed = 0;
            throw new BrakeException();
        }
        return currentSpeed;
    }

    public void drive(int distance) throws GasLackException, WrongDataException {
        if (distance < 0) {
            throw new WrongDataException();
        }
        getGas(distance);
    }

    public void tankUp(int amount) throws GasOverflowException, WrongDataException {
        if (gasAmount + amount <= TANK_CAPACITY && amount > 0) {
            gasAmount += amount;
        } else if (amount < 0) {
            throw new WrongDataException();
        } else {
            throw new GasOverflowException(TANK_CAPACITY);
        }
    }

    public double getGasAmount() {
        return gasAmount;
    }

    private void getGas(int distance) throws GasLackException {
        if (gasAmount - (distance * FUEL_CONSUMPTION_PER_100_KM) / 100 >= 0) {
            gasAmount -= (distance * FUEL_CONSUMPTION_PER_100_KM) / 100;
        } else {
            throw new GasLackException(distance);
        }
    }
}
