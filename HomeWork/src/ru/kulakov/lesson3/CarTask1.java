package ru.kulakov.lesson3;

public class CarTask1 implements Comparable<CarTask1> {
    int price;

    CarTask1(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int compareTo(CarTask1 car) {
        if (this.price > car.getPrice()) {
            return 1;
        } else if (this.price < car.getPrice()) {
            return -1;
        }
        return 0;
    }
}
