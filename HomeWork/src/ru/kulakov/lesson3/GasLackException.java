package ru.kulakov.lesson3;

public class GasLackException extends Exception {
    private int distance;

    public GasLackException(int distance) {
        this.distance = distance;
    }

    @Override
    public String getMessage() {
        return "There aren't enough fuel to drive " + distance + " km.";
    }
}
