package ru.kulakov.lesson3;

public class WrongDataException extends Exception {
    @Override
    public String getMessage() {
        return "Entered data is wrong! Use only positive numbers.";
    }
}
