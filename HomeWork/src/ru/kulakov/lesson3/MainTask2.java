package ru.kulakov.lesson3;

public class MainTask2 {
    private final static int ITERATIONS_COUNT = 100000;

    public static void main(String[] args) {
        String string = new String("");
        StringBuilder stringBuilder = new StringBuilder("");
        StringBuffer stringBuffer = new StringBuffer("");

        long currentTime = System.currentTimeMillis();
        for (int i = 0; i < ITERATIONS_COUNT; i++) {
            string += "someText";
        }
        System.out.println("Result for String: " + (System.currentTimeMillis() - currentTime)); //result is 63460 ms

        currentTime = System.currentTimeMillis();
        for (int i = 0; i < ITERATIONS_COUNT; i++) {
            stringBuilder.append("anotherText"); // using another text to prevent cac hing
        }
        string = stringBuilder.toString();
        System.out.println("Result for StringBuilder: " + (System.currentTimeMillis() - currentTime)); //14 ms

        currentTime = System.currentTimeMillis();
        for (int i = 0; i < ITERATIONS_COUNT; i++) {
            stringBuffer.append("moreText");
        }
        string = stringBuffer.toString();
        System.out.println("Result for StringBuffer: " + (System.currentTimeMillis() - currentTime)); //16 ms
    }
}
