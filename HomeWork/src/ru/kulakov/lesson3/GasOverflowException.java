package ru.kulakov.lesson3;

public class GasOverflowException extends Exception {
    private double tankCapacity;

    public GasOverflowException(double tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    @Override
    public String getMessage() {
        return "You've tried to fill tank with more then " + tankCapacity + ". It is dangerous!";
    }
}
