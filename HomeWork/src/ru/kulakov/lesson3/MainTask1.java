package ru.kulakov.lesson3;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainTask1 {
    private static final int ARRAY_LENGTH = 10;

    public static void main(String[] args) {
        CarTask1[] cars = new CarTask1[ARRAY_LENGTH];

        for (int i = 0; i < ARRAY_LENGTH; i++) {                      //Fill array
            cars[i] = new CarTask1((int) (Math.random() * 5000 + 5000));
        }

        CarTask1[] sortedCars = (CarTask1[]) sort(cars);

        for (int i = 0; i < ARRAY_LENGTH; i++) {                      //Shows that first array didn't change
            System.out.println(cars[i].getPrice() + " $");
        }

        System.out.println("___________________________");
        for (int i = 0; i < ARRAY_LENGTH; i++) {                      //Shows new array
            System.out.println(sortedCars[i].getPrice() + " $");
        }

    }

    public static Comparable[] sort(Comparable[] elements) {
        elements = elements.clone();
        Arrays.sort(elements);
        return elements;
    }

}
