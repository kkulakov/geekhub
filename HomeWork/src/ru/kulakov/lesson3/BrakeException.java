package ru.kulakov.lesson3;

public class BrakeException extends Exception {
    @Override
    public String getMessage() {
        return "You've tried to move less then 0 km/h. The speed is 0 km/h now.";
    }
}
