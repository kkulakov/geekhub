package ru.kulakov.lesson1;

import java.util.Scanner;

public class FibonacciNumbers {
    public static void main(String[] args) {
        System.out.println("Enter a count of Fibonacci numbers to show:");
        try {
            Scanner in = new Scanner(System.in);
            int number = Integer.parseInt(in.next());
            if (number > 0)
                System.out.println("" + getFibonacciNumbers(number));
            else
                System.out.println("Enter positive number!");
        } catch (NumberFormatException exception) {
            System.out.println("Enter correct number!");
        }
    }

    public static String getFibonacciNumbers(int count) {
        String res = "0";
        int num0 = 0,
                num1 = 1,
                num2;
        for (int i = 1; i < count; i++) {
            num2 = num1 + num0;
            num0 = num1;
            num1 = num2;
            res += ", " + num2;
        }
        return res + ".";
    }
}
