package ru.kulakov.lesson1;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        System.out.println("To calculate factorial enter positive number:");
        try {
            Scanner in = new Scanner(System.in);
            int number = Integer.parseInt(in.next());
            if (number >= 0)
                System.out.println("" + calculateFactorial(number));
            else
                System.out.println("Enter positive number!");
        } catch (NumberFormatException exception) {
            System.out.println("Enter correct number!");
        }
    }

    public static long calculateFactorial(int number) {
        long res = 1;
        for (int i = 1; i <= number; i++)
            res *= i;
        return res;
    }
}
