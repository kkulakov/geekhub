package ru.kulakov.lesson5;

import ru.kulakov.lesson5.source.SourceLoader;
import ru.kulakov.lesson5.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator();

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) {
            //      So, the only way to stop the application is to do that manually or type "exit"
            String source = null;
            try {
                source = sourceLoader.loadSource(command);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Entered path is wrong! Try again.");
            }
            String translation = null;
            try {
                translation = translator.translate(source);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Can't translate text!");
            }

            System.out.println("Original: " + source);
            System.out.println("Translation: " + translation);

            command = scanner.next();
        }
    }
}
