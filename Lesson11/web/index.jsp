<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Translator</title>
    </head>
    <body>
        <form action="" method="POST">
            <table cellspacing="10px">
                <tr>
                    <td colspan="2" width="50%"> <textarea rows="10" style="width: 100%;" name="source" autofocus></textarea>
                    <td width="50%">${result}
                </tr>
                <tr>
                    <td>
                    <td width="5%"><input type="submit" value="Translate">
                    <td>
                </tr>
            </table>
        </form>
    </body>
</html>