package ru.kulakov.lesson11;


import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.kulakov.lesson11.Translator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "TranslatorServlet", urlPatterns = "")
public class TranslatorServlet extends HttpServlet {

    Translator translator = new ClassPathXmlApplicationContext("application-context.xml").getBean(Translator.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String result;
        result = translator.translate(request.getParameter("source"));
        request.setAttribute("result", result);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
