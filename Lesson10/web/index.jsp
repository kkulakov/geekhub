<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/listFormatter.tld" prefix="lf" %>
<html>
  <head>
    <title>Table</title>
  </head>
  <body>
      <jsp:useBean id="animals" scope="session" type="java.util.List"/>
      <lf:listFormat animals="${animals}"/>
  </body>
</html>