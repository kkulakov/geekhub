package ru.kulakov.lesson10;

public class Animal implements Comparable<Animal>{
    private int id;
    private String name;
    private String value;

    public Animal(int id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int compareTo(Animal animal) {
        return (name == null) ? -1 : name.compareTo(animal.getName());
    }
}
