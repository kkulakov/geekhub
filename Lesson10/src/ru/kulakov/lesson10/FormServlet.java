package ru.kulakov.lesson10;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name="FormServlet", urlPatterns = "")
public class FormServlet extends HttpServlet {
    //Adding new element
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        List<Animal> animals;
        if (session.isNew()){
            animals = new ArrayList<>();
        } else {
            animals = (List<Animal>)session.getAttribute("animals");
        }

        int id = (int) session.getAttribute("lastID");
        String name =  request.getParameter("name");
        String value = request.getParameter("value");
        animals.add(new Animal(id , name, value));

        session.setAttribute("lastID", id + 1);
        session.setAttribute("animals", animals);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }


    //Showing and deleting elements
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        List<Animal> animals;
        if (session.isNew()){
            animals = new ArrayList<>();
            session.setAttribute("lastID", 0);
        } else {
            animals = (List<Animal>)session.getAttribute("animals");
        }

        try{
            int deleteID = Integer.parseInt(request.getParameter("delete"));
            for(Animal animal : animals){
                if(animal.getId() == deleteID){
                    animals.remove(animal);
                    break;
                }
            }
        } catch(NumberFormatException e) {
            //It's OK
        }
        session.setAttribute("animals", animals);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
