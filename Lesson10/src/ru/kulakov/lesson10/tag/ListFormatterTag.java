package ru.kulakov.lesson10.tag;

import ru.kulakov.lesson10.Animal;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class ListFormatterTag extends TagSupport {

    private ArrayList<Animal> animals;

    public ArrayList<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(ArrayList<Animal> animals) {
        this.animals = animals;

    }

    public int doStartTag() throws JspException {
        try {
            JspWriter out = super.pageContext.getOut();
            out.println("<form action=\"\" method=\"POST\">");
            out.println("<table border><tr><th width=\"100px\">Name<th width=\"100px\">Value<th>Action</tr>");

            Collections.sort(animals);
            for (int i = 0; i< animals.size(); i++){
                out.println("<tr>");
                if(i == 0 || animals.get(i - 1).compareTo(animals.get(i)) != 0)
                    out.println("<td>" + animals.get(i).getName());
                else
                    out.println("<td>");
                out.println("<td>" + animals.get(i).getValue());
                out.println("<td><a href=\"?delete=" + animals.get(i).getId() + "\">delete</a>");
                out.println("</tr>");
            }

            out.println("<tr>");
            out.println("<td><input type=\"text\" name=\"name\">");
            out.println("<td><input type=\"text\" name=\"value\">");
            out.println("<td><input type=\"submit\" value=\"add\">");
            out.println("</tr>");

            out.println("</table>");
            out.println("</form>");

        } catch(IOException ioe) {
            throw new JspException("Error: " + ioe.getMessage());
        }
        return SKIP_BODY;
    }



}
