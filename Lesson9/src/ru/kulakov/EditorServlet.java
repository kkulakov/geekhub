package ru.kulakov;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet(name = "EditorServlet", urlPatterns = "/edit")
public class EditorServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filePath = request.getParameter("file");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filePath)))) {
            writer.write(request.getParameter("changedText"));
        }
        response.sendRedirect("/?folder=" + filePath.substring(0, filePath.lastIndexOf('/')));
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filePath = request.getParameter("file");
        String fileName = filePath.substring(filePath.lastIndexOf('/') + 1);

        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line = reader.readLine();

            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line = reader.readLine();
            }
        }

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>" + fileName + "</title></head><body>");
        out.println("<h2>" + fileName + "</h2>");
        out.println("<form action=\"/edit?file=" + filePath + "\" method=\"POST\">");
        out.println("<textarea autofocus rows=\"20\" cols=\"120\" name=\"changedText\">");
        out.println(stringBuilder.toString());
        out.println("</textarea> <br><input type=\"submit\" value=\"Save\"/> </form>");
        out.print("</body></html>");
        out.close();
    }
}
