package ru.kulakov;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "ExplorerServlet", urlPatterns = "/")
public class ExplorerServlet extends HttpServlet {
    String rootDirectory;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        rootDirectory = config.getInitParameter("rootDirectory");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String folderIcon = "<img src = \"http://icons.iconarchive.com/icons/hopstarter/sleek-xp-basic/16/Folder-icon.png\"/> ";
        String txtIcon = "<img src = \"http://icons.iconarchive.com/icons/saki/nuoveXT-2/16/Mimetypes-txt-icon.png\"/> ";
        String otherIcon = "<img src = \"http://icons.iconarchive.com/icons/hopstarter/sleek-xp-basic/16/Help-icon.png\"/> ";
        String deleteIcon = "<img src = \"http://icons.iconarchive.com/icons/oxygen-icons.org/oxygen/16/Actions-edit-delete-icon.png\"/> ";

        String currentDirectory;
        if ((currentDirectory = request.getParameter("folder")) == null) {
            currentDirectory = rootDirectory;
        }

        String deleteFile;
        if ((deleteFile = request.getParameter("delete")) != null) {
            new File(currentDirectory + "/" + deleteFile).delete();
        }

        File[] files = new File(currentDirectory).listFiles();


        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>" + currentDirectory + "</title></head><body>");

        if (!currentDirectory.equalsIgnoreCase(rootDirectory)) {
            out.println("<h1>" + currentDirectory.substring(currentDirectory.lastIndexOf('/')) + "</h1>");
            out.println("<a href=\"/?folder=" + currentDirectory.substring(0, currentDirectory.lastIndexOf('/'))
                    + "\"> ... </a><br>");
        } else {
            out.println("<h1>root</h1>");
        }

        if (files == null) {
            out.print("</body></html>");
            out.close();
            return;
        }

        for (File file : files) {  //showing directories first
            if (file.isDirectory()) {
                out.println("<a href=\"/?folder=" + currentDirectory + "/" + file.getName() + "\">"
                        + folderIcon + file.getName() + "</a>");
                out.print("<a href=\"/?folder=" + currentDirectory + "&delete=" + file.getName() + "\">" +
                        deleteIcon + "</a><br>");
            }
        }
        for (File file : files) {
            if (!file.isFile()) continue;
            if (".txt".equalsIgnoreCase(file.toString().substring(file.toString().lastIndexOf('.')))) {//looking for txt files
                out.println("<a href=\"/edit?file=" + currentDirectory + "/" + file.getName() + "\">"
                        + txtIcon + file.getName() + "</a>");
            } else {
                out.println(otherIcon + file.getName());
            }
            out.print("<a href=\"/?folder=" + currentDirectory + "&delete=" + file.getName() + "\">" +
                    deleteIcon + "</a><br>");
        }

        out.println("<div align=\"right\"><a href=\"/create?type=folder&place=" + currentDirectory
                + "\">Create directory</a></div>");
        out.println("<div align=\"right\"><a href=\"/create?type=txt&place=" + currentDirectory
                + "\">Create txt file</a></div>");

        out.print("</body></html>");
        out.close();
    }

}
