package ru.kulakov;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ElementCreatorServlet", urlPatterns = "/create")
public class ElementCreatorServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentDirectory = request.getParameter("place");
        String filePath = currentDirectory + "/" + request.getParameter("fileName");
        String elementType = request.getParameter("type");

        if ("txt".equals(elementType)) {
            filePath += ".txt";
            new File(filePath).createNewFile();
        } else if ("folder".equals(elementType)) {
            new File(filePath).mkdirs();
        }
        response.sendRedirect("/?folder=" + currentDirectory);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentDirectory = request.getParameter("place");
        String elementType = request.getParameter("type");
        if (currentDirectory == null || elementType == null) {
            response.sendRedirect("/");
        }

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>" + currentDirectory + "</title></head><body>");
        out.println("<form action=\"/create?place=" + currentDirectory
                + "&type=" + elementType + "\" method=\"POST\"><input type=\"text\" name=\"fileName\">");
        out.println("</input> <br><input type=\"submit\" value=\"Create\"/> </form>");
        out.print("</body></html>");
        out.close();
    }
}
